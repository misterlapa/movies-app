# Movies App - Prueba tecnica para Rappi

## Link hacia APK

[APK Movies App](https://drive.google.com/file/d/1C17vAtyjy4gurg_TY9B189dbkfgNXrle/view?usp=sharing)

## Descripcion de capas de la aplicacion y clases pertenecientes

1. Capas de la aplicacion:

- 1.1 Capa de persistencia: Se utiliza Room para realizar la funcionalidad de la persistencia junto con Paging para realizar la paginacion segun los datos que se soliciten por el usuario.
Estas clases se utilizan para realizar la funcionalidad de persistencia creando el modelo de datos y los DAOs para acceder a los datos.
Las clases pertenecientes a esta capa son las siguientes:

Category, 
CategoryType, 
Dates, 
Genre, 
Info, 
InfoGenre, 
InfoVideo,
Movie,
MovieVideo,
CategoryDAO,
MovieDAO,
MovieDatabase,
DBMovieDataSource,
DBMovieDataSourceFactory,
MovieRepository


- 1.2 Capa de Red: Se utiliza Retrofit para realizar el consumo de los web services junto con Paging para realizar el paginado.
Estas clases se utiliza para crear la funcionalidad de consumo de web service haciendo el llamado a la API y endpoints.
Las clases pertenecientes a esta capa son las siguientes:

ApiUtils,
RetrofitClient,
NetMovieDataSource,
NetMovieDataSourceFactory,
ApiClient

- 1.3 Capa de negocio: En la capa de negocio se utiliza el patron de diseno arquitectonico MVVM
En esta clase se realiza el llamado a el repositorio de datos, segun el filtrado solicitado. Tambien se hace el llamado a los detalles por pelicula.
Las clases pertenecientes a esta capa son las siguientes:

DetailMovieActivityViewModel,
MainActivityViewModel

- 1.4 Capa de presentacion:
En esta capa estan las clases encargadas del manejo de activities y mostrar la interfaz al usuario.
Las clases pertenecientes a esta capa son las siguientes:

DetailMovieActivity,
MainActivity


## En que consiste el principio de responsabilidad unica? Cual es su proposito?

Establece que cada clase debe tener responsabilidad sobre una sola parte de la funcionalidad proporcionada por el software y esta responsabilidad debe estar encapsulada en su totalidad por la clase.

## Que caracteristicas tiene, segun su opinion, un "buen" codigo o codigo limpio?

- Debe ser enfocado, es decir deberia cumplir con el principio de Responsabilidad Unica.
- Puede ser extendido facilmente por cualquier otro desarrollador.
- Debe poseer un codigo facil de leer.
- Debe tener dependencias minimas.
- No debe ser redundante, la modificacion de un unico elemento del sistema no debe requerir cambios en otros elementos que no tengan relacion logica.
