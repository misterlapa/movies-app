package com.example.moviesapp.network.paging

import android.arch.lifecycle.MutableLiveData
import android.arch.paging.DataSource
import com.example.moviesapp.storage.model.CategoryType
import com.example.moviesapp.storage.model.Movie
import com.example.moviesapp.storage.repository.MovieRepository

class NetMovieDataSourceFactory(private val categoryType: CategoryType, private val movieRepository: MovieRepository) :
    DataSource.Factory<Int, Movie>() {

    private lateinit var netMovieDataSource: NetMovieDataSource
    var mutableLiveDataNet: MutableLiveData<NetMovieDataSource> = MutableLiveData()

    override fun create(): DataSource<Int, Movie> {

        netMovieDataSource = NetMovieDataSource(categoryType, movieRepository)
        mutableLiveDataNet.postValue(netMovieDataSource)

        return netMovieDataSource
    }
}