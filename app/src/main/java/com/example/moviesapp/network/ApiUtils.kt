package com.example.moviesapp.network

import com.example.moviesapp.request.ApiClient

class ApiUtils {
    companion object {
        const val BASE_URL = "https://api.themoviedb.org/3/"
        const val apiKey = "7b4164c643aa660eb545793653b5e3a7"

        fun getAPIClient(): ApiClient? {
            return RetrofitClient.getRetrofitClient(BASE_URL)?.create(ApiClient::class.java)
        }
    }
}