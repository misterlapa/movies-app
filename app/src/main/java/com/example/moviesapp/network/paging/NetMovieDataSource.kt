package com.example.moviesapp.network.paging

import android.arch.paging.PageKeyedDataSource
import com.example.moviesapp.storage.model.CategoryType
import com.example.moviesapp.storage.model.Info
import com.example.moviesapp.storage.model.Movie
import com.example.moviesapp.network.ApiUtils
import com.example.moviesapp.storage.repository.MovieRepository
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NetMovieDataSource(private val categoryType: CategoryType, private val movieRepository: MovieRepository) :
    PageKeyedDataSource<Int, Movie>() {

    private var apiClient = ApiUtils.getAPIClient()

    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, Movie>) {

        var callData: Call<Info>? = null

        when (categoryType.name) {
            CategoryType.Popular.toString() -> callData = apiClient?.getPopularMovies(ApiUtils.apiKey, 1)
            CategoryType.TopRated.toString() -> callData = apiClient?.getTopRatedMovies(ApiUtils.apiKey, 1)
            CategoryType.Upcoming.toString() -> callData = apiClient?.getUpcomingMovies(ApiUtils.apiKey, 1)
        }

        if (callData == null) {
            return
        }

        callData.enqueue(object : Callback<Info> {
            override fun onFailure(call: Call<Info>, t: Throwable) {

            }

            override fun onResponse(call: Call<Info>, response: Response<Info>) {

                val responseInfo = response.body()
                val moviesList = responseInfo?.movies as List<Movie>
                saveMovies(moviesList)
                callback.onResult(moviesList, null, 2)
            }
        })
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Movie>) {

        var callData: Call<Info>? = null

        when (categoryType.name) {
            CategoryType.Popular.toString() -> callData = apiClient?.getPopularMovies(ApiUtils.apiKey, params.key)
            CategoryType.TopRated.toString() -> callData = apiClient?.getTopRatedMovies(ApiUtils.apiKey, params.key)
            CategoryType.Upcoming.toString() -> callData = apiClient?.getUpcomingMovies(ApiUtils.apiKey, params.key)
        }

        if (callData == null) {
            return
        }

        callData.enqueue(object : Callback<Info> {
            override fun onFailure(call: Call<Info>, t: Throwable) {

            }

            override fun onResponse(call: Call<Info>, response: Response<Info>) {

                val responseInfo = response.body()
                val moviesList = responseInfo?.movies as List<Movie>
                saveMovies(moviesList)
                callback.onResult(moviesList, params.key + 1)
            }
        })
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Movie>) {

    }

    private fun saveMovies(movies: List<Movie>) {
        if (movies.isNotEmpty()) {
            movieRepository.insertMovieList(movies, categoryType)
        }
    }
}