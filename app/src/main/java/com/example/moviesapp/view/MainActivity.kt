package com.example.moviesapp.view

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.support.v4.content.ContextCompat
import android.support.v7.widget.GridLayoutManager
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.example.moviesapp.R
import com.example.moviesapp.adapter.MovieAdapter
import com.example.moviesapp.storage.model.Category
import com.example.moviesapp.storage.model.CategoryType
import com.example.moviesapp.storage.model.Movie
import com.example.moviesapp.viewmodel.MainActivityViewModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var mainActivityViewModel: MainActivityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        srMovies.setColorSchemeColors(
            ContextCompat.getColor(
                this,
                R.color.colorPrimary
            )
        )

        mainActivityViewModel = ViewModelProviders.of(this).get(MainActivityViewModel::class.java)

        mainActivityViewModel.connectedToNetwork = isNetworkAvailable()

        if (mainActivityViewModel.lastCategory == null) {
            mainActivityViewModel.lastCategory = CategoryType.values()[spinnerCategories.selectedItemPosition]
            mainActivityViewModel.buildMoviePagedLive(mainActivityViewModel.lastCategory!!)
        }

        loadRecyclerView()

        mainActivityViewModel.getAllCategories().observe(this, Observer {
            if (it != null) {
                loadSpinnerItems(it)
            }
        })

        spinnerCategories.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                val selectedCategory = CategoryType.values()[position]

                if (mainActivityViewModel.lastCategory == selectedCategory) {
                    return
                }

                mainActivityViewModel.lastCategory = selectedCategory
                mainActivityViewModel.buildMoviePagedLive(selectedCategory)
                loadRecyclerView()
            }
        }

        srMovies.setOnRefreshListener {
            mainActivityViewModel.connectedToNetwork = isNetworkAvailable()
            mainActivityViewModel.buildMoviePagedLive(mainActivityViewModel.lastCategory!!)
            loadRecyclerView()
            Handler().postDelayed({

                srMovies.isRefreshing = false
            }, 2000)
        }

        rvMovies.layoutManager = GridLayoutManager(this, 2)
    }

    private fun loadRecyclerView() {

        val liveData = mainActivityViewModel.moviePagedLive

        liveData.observe(this, Observer {

            if (it != null) {
                val movieAdapter = MovieAdapter(applicationContext)
                movieAdapter.submitList(it)
                movieAdapter.setOnClickItem(object : MovieAdapter.ClickItem {
                    override fun setOnClickItem(movie: Movie) {
                        val intent = Intent(applicationContext, DetailMovieActivity::class.java)
                        intent.putExtra(DetailMovieActivity.MOVIE_INTENT, movie)
                        startActivity(intent)
                    }
                })

                rvMovies.adapter = movieAdapter
            }
        })
    }

    private fun loadSpinnerItems(items: List<Category>) {
        val arrayAdapter = ArrayAdapter<Category>(this, android.R.layout.simple_spinner_item, items)
        arrayAdapter.setDropDownViewResource(R.layout.spinner_item)
        spinnerCategories.adapter = arrayAdapter
    }

    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }
}
