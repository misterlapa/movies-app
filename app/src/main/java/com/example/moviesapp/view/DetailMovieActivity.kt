package com.example.moviesapp.view

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.bumptech.glide.Glide
import com.example.moviesapp.R
import com.example.moviesapp.storage.model.Genre
import com.example.moviesapp.storage.model.InfoGenre
import com.example.moviesapp.storage.model.InfoVideo
import com.example.moviesapp.storage.model.Movie
import com.example.moviesapp.viewmodel.DetailMovieActivityViewModel

import kotlinx.android.synthetic.main.activity_detail_movie.*
import kotlinx.android.synthetic.main.content_detail_movie.*

class DetailMovieActivity : AppCompatActivity() {

    private lateinit var detailMovieActivityViewModel: DetailMovieActivityViewModel
    private lateinit var infoVideoLiveData: LiveData<InfoVideo>
    private lateinit var infoGenreLiveData: LiveData<InfoGenre>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_movie)
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        if (!intent.hasExtra(MOVIE_INTENT)) {
            return
        }

        val movie: Movie? = intent.extras.getParcelable(MOVIE_INTENT)
        val imagePath = "https://image.tmdb.org/t/p/w500/" + movie!!.posterPath

        Glide.with(this)
            .load(imagePath)
            .placeholder(R.drawable.loading)
            .into(ivPoster)

        supportActionBar?.title = null

        tvMovieTitle.text = movie.originalTitle

        if (movie.voteAverage != null) {
            tvMovieRating.text = movie.voteAverage.toString()
        }

        tvSynopsis.text = movie.overview
        tvLanguage.text = getString(R.string.language, movie.originalLanguage)
        tvReleaseDate.text = movie.releaseDate

        detailMovieActivityViewModel = ViewModelProviders.of(this).get(DetailMovieActivityViewModel::class.java)
        infoVideoLiveData = detailMovieActivityViewModel.getInfoVideoLiveData()

        infoVideoLiveData.observe(this, Observer {

            if (it != null) {

                val trailerUrl = getVideoLink(it)
                if (trailerUrl != null) {
                    tvTrailer.tag = trailerUrl
                    tvTrailer.visibility = View.VISIBLE
                }
            } else {
                movie.idMovie?.let { it1 -> detailMovieActivityViewModel.getInfoVideoData(it1) }
            }
        })

        infoGenreLiveData = detailMovieActivityViewModel.getInfoGenreLiveData()
        infoGenreLiveData.observe(this, Observer {

            if (it != null) {

                val concatenatedCategories = getConcatenatedCategories(it, movie)
                if (concatenatedCategories != null) {
                    tvCategories.text = concatenatedCategories
                    tvCategories.visibility = View.VISIBLE
                }
            } else {
                movie.idMovie?.let { detailMovieActivityViewModel.getInfoGenreData() }
            }
        })

        tvTrailer.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(tvTrailer.tag.toString()))
            startActivity(intent)
        }
    }

    companion object {
        const val MOVIE_INTENT = "movie"
        private const val YOUTUBE_URL = "https://www.youtube.com/watch?v="
    }

    private fun getVideoLink(infoVideo: InfoVideo): String? {

        if (infoVideo.results == null) {
            return null
        }

        if (infoVideo.results!!.size < 1) {
            return null
        }

        val movieVideo = infoVideo.results!!.first { it.type == "Trailer" }
        return YOUTUBE_URL + movieVideo.key
    }

    private fun getConcatenatedCategories(infoGenre: InfoGenre, movie: Movie): String? {

        val foundCategories = ArrayList<Genre>()
        var concatenatedCategories: String? = null

        if (infoGenre.genres == null) {
            return concatenatedCategories
        }

        if (infoGenre.genres!!.size < 1) {
            return concatenatedCategories
        }

        if (movie.genreIds == null) {
            return concatenatedCategories
        }

        if (movie.genreIds!!.size < 1) {
            return concatenatedCategories
        }

        infoGenre.genres!!.forEach {
            if (movie.genreIds!!.contains(it.id)) {
                foundCategories.add(it)
            }
        }

        if (foundCategories.size > 0) {
            concatenatedCategories = foundCategories.joinToString(", ") {
                it.name
            }
        }

        return concatenatedCategories
    }
}
