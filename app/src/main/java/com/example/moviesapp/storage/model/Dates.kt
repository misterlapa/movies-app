package com.example.moviesapp.storage.model

import com.google.gson.annotations.SerializedName

class Dates {

    @SerializedName("maximum")
    lateinit var maximum: String
    @SerializedName("minimum")
    lateinit var minimum: String
}