package com.example.moviesapp.storage

import android.arch.persistence.db.SupportSQLiteDatabase
import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import android.os.AsyncTask
import com.example.moviesapp.storage.model.Category
import com.example.moviesapp.storage.model.CategoryType
import com.example.moviesapp.storage.model.Movie

@Database(entities = [Category::class, Movie::class], version = 3)
abstract class MovieDatabase : RoomDatabase() {

    abstract fun getCategoryDAO(): CategoryDAO
    abstract fun getMovieDAO(): MovieDAO

    companion object {
        var instance: MovieDatabase? = null

        fun getInstance(context: Context): MovieDatabase {

            if (instance != null) {
                return instance as MovieDatabase
            }

            instance = Room.databaseBuilder(context, MovieDatabase::class.java, "movie_database")
                .fallbackToDestructiveMigration()
                .addCallback(callback)
                .build()

            return instance as MovieDatabase
        }

        private var callback = object : Callback() {
            override fun onCreate(db: SupportSQLiteDatabase) {
                super.onCreate(db)
                instance?.let {
                    GetInitialDataAsyncTask(
                        it
                    ).execute()
                }
            }
        }

        class GetInitialDataAsyncTask(movieDatabase: MovieDatabase) : AsyncTask<Void, Void, Void>() {

            private var categoryDAO: CategoryDAO = movieDatabase.getCategoryDAO()

            override fun doInBackground(vararg params: Void?): Void? {

                val listCategory = ArrayList<Category>()

                for (item in CategoryType.values()) {

                    val category = Category()
                    category.id = item.value
                    category.name = item.name

                    listCategory.add(category)
                }

                categoryDAO.addCategoryList(listCategory)

                return null
            }
        }
    }
}