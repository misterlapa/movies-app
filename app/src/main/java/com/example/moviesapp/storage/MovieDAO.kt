package com.example.moviesapp.storage

import android.arch.persistence.room.*
import com.example.moviesapp.storage.model.Movie

@Dao
interface MovieDAO {

    @Insert
    fun addMovie(movie: Movie)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addMovieList(movie: List<Movie>)

    @Update
    fun updateMovie(movie: Movie)

    @Delete
    fun deleteBook(movie: Movie)

    @Query("SELECT * FROM movie WHERE id_category ==:idCategory")
    fun queryMoviesByCategory(idCategory: Int): List<Movie>
}