package com.example.moviesapp.storage.model

import com.google.gson.annotations.SerializedName

class Genre {
    @SerializedName("id")
    var id: Int? = null
    @SerializedName("name")
    lateinit var name: String
}