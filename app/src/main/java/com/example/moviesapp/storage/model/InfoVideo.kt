package com.example.moviesapp.storage.model

import com.google.gson.annotations.SerializedName

class InfoVideo {

    @SerializedName("id")
    var id: Int? = null
    @SerializedName("results")
    var results : ArrayList<MovieVideo>? = null
}