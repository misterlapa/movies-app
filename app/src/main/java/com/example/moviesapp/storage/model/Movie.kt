package com.example.moviesapp.storage.model

import android.arch.persistence.room.*
import android.os.Parcel
import android.os.Parcelable
import android.support.v7.util.DiffUtil
import com.google.gson.annotations.SerializedName

@Entity(
    tableName = "movie", foreignKeys = [
        ForeignKey(
            entity = Category::class,
            parentColumns = ["id"],
            childColumns = ["id_category"],
            onDelete = ForeignKey.CASCADE
        )
    ]
)
class Movie() : Parcelable {

    @ColumnInfo(name = "poster_path")
    @SerializedName("poster_path")
    var posterPath: String? = null

    @ColumnInfo(name = "adult")
    @SerializedName("adult")
    var adult: Boolean? = null

    @ColumnInfo(name = "overview")
    @SerializedName("overview")
    var overview: String? = null

    @ColumnInfo(name = "release_date")
    @SerializedName("release_date")
    var releaseDate: String? = null

    @Ignore
    @SerializedName("genre_ids")
    var genreIds: ArrayList<Int>? = null

    @PrimaryKey
    @ColumnInfo(name = "id")
    @SerializedName("id")
    var idMovie: Long? = null

    @ColumnInfo(name = "original_title")
    @SerializedName("original_title")
    var originalTitle: String? = null

    @ColumnInfo(name = "original_language")
    @SerializedName("original_language")
    var originalLanguage: String? = null

    @ColumnInfo(name = "title")
    @SerializedName("title")
    var title: String? = null

    @ColumnInfo(name = "backdrop_path")
    @SerializedName("backdrop_path")
    var backdropPath: String? = null

    @ColumnInfo(name = "popularity")
    @SerializedName("popularity")
    var popularity: Double? = null

    @ColumnInfo(name = "vote_count")
    @SerializedName("vote_count")
    var voteCount: Int? = null

    @ColumnInfo(name = "video")
    @SerializedName("video")
    var video: Boolean? = null

    @ColumnInfo(name = "vote_average")
    @SerializedName("vote_average")
    var voteAverage: Double? = null

    @ColumnInfo(name = "id_category")
    var idCategory: Int = 1

    constructor(parcel: Parcel) : this() {
        posterPath = parcel.readString()
        adult = parcel.readValue(Boolean::class.java.classLoader) as? Boolean
        overview = parcel.readString()
        releaseDate = parcel.readString()
        idMovie = parcel.readValue(Long::class.java.classLoader) as? Long
        originalTitle = parcel.readString()
        originalLanguage = parcel.readString()
        title = parcel.readString()
        backdropPath = parcel.readString()
        popularity = parcel.readValue(Double::class.java.classLoader) as? Double
        voteCount = parcel.readValue(Int::class.java.classLoader) as? Int
        video = parcel.readValue(Boolean::class.java.classLoader) as? Boolean
        voteAverage = parcel.readValue(Double::class.java.classLoader) as? Double
        genreIds = parcel.readArrayList(ArrayList::class.java.classLoader) as? ArrayList<Int>
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(posterPath)
        parcel.writeValue(adult)
        parcel.writeString(overview)
        parcel.writeString(releaseDate)
        parcel.writeValue(idMovie)
        parcel.writeString(originalTitle)
        parcel.writeString(originalLanguage)
        parcel.writeString(title)
        parcel.writeString(backdropPath)
        parcel.writeValue(popularity)
        parcel.writeValue(voteCount)
        parcel.writeValue(video)
        parcel.writeValue(voteAverage)
        parcel.writeList(genreIds)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Movie> {
        override fun createFromParcel(parcel: Parcel): Movie {
            return Movie(parcel)
        }

        override fun newArray(size: Int): Array<Movie?> {
            return arrayOfNulls(size)
        }

        var CALLBACK = object : DiffUtil.ItemCallback<Movie>() {
            override fun areItemsTheSame(p0: Movie, p1: Movie): Boolean {
                return p0.idMovie == p1.idMovie
            }

            override fun areContentsTheSame(p0: Movie, p1: Movie): Boolean {
                return true
            }
        }
    }
}