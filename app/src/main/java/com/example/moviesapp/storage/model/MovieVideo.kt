package com.example.moviesapp.storage.model

import com.google.gson.annotations.SerializedName

class MovieVideo {

    @SerializedName("id")
    lateinit var id: String
    @SerializedName("iso_639_1")
    lateinit var iso6391: String
    @SerializedName("iso_3166_1")
    lateinit var iso31661: String
    @SerializedName("key")
    lateinit var key: String
    @SerializedName("name")
    lateinit var name: String
    @SerializedName("site")
    lateinit var site: String
    @SerializedName("size")
    var size: Int? = null
    @SerializedName("type")
    lateinit var type: String
}