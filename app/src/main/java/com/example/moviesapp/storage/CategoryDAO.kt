package com.example.moviesapp.storage

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import com.example.moviesapp.storage.model.Category

@Dao
interface CategoryDAO {

    @Insert
    fun addCategory(category: Category)

    @Insert
    fun addCategoryList(category: List<Category>)

    @Update
    fun updateCategory(category: Category)

    @Delete
    fun deleteCategory(category: Category)

    @Query("select * from movie_category")
    fun queryAllCategories(): LiveData<List<Category>>
}