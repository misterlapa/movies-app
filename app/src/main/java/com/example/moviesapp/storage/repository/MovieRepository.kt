package com.example.moviesapp.storage.repository

import android.app.Application
import android.arch.lifecycle.LiveData
import android.os.AsyncTask
import com.example.moviesapp.storage.MovieDatabase
import com.example.moviesapp.storage.model.Category
import com.example.moviesapp.storage.model.CategoryType
import com.example.moviesapp.storage.model.Movie

class MovieRepository(application: Application) {

    private var movieDatabase = MovieDatabase.getInstance(application)
    private var categoryDAO = movieDatabase.getCategoryDAO()
    private var movieDAO = movieDatabase.getMovieDAO()
    private lateinit var movieList: List<Movie>

    fun getAllCategories(): LiveData<List<Category>> {
        return categoryDAO.queryAllCategories()
    }

    fun getMoviesByCategory(categoryType: CategoryType): List<Movie> {
        movieList = movieDAO.queryMoviesByCategory(categoryType.value)
        return movieList
    }

    fun insertMovieList(movieList: List<Movie>, categoryType: CategoryType) {

        movieList.forEach {
            it.idCategory = categoryType.value
        }

        InsertMovieAsyncTask().execute(movieList)
    }

    private inner class InsertMovieAsyncTask : AsyncTask<List<Movie>, Void, Void>() {
        override fun doInBackground(vararg params: List<Movie>?): Void? {
            params[0]?.let { movieDAO.addMovieList(it) }
            return null
        }
    }

    private inner class GetMoviesByCategoryAsyncTask : AsyncTask<CategoryType, Void, Void>() {
        override fun doInBackground(vararg params: CategoryType?): Void? {
            movieList = params[0]?.value?.let { movieDAO.queryMoviesByCategory(it) }!!
            return null
        }
    }
}