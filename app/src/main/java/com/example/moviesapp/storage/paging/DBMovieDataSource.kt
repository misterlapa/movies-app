package com.example.moviesapp.storage.paging

import android.arch.paging.PageKeyedDataSource
import com.example.moviesapp.storage.model.CategoryType
import com.example.moviesapp.storage.model.Movie
import com.example.moviesapp.storage.repository.MovieRepository

class DBMovieDataSource(private val categoryType: CategoryType, private val movieRepository: MovieRepository) :
    PageKeyedDataSource<Int, Movie>() {

    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, Movie>) {
        val movieList = movieRepository.getMoviesByCategory(categoryType)
        callback.onResult(movieList, null, 2)
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Movie>) {
        val movieList = movieRepository.getMoviesByCategory(categoryType)
        callback.onResult(movieList, params.key + 1)
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Movie>) {

    }
}