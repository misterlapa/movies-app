package com.example.moviesapp.storage.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "movie_category")
class Category {

    @PrimaryKey
    @ColumnInfo(name = "id")
    var id: Int = 1

    @ColumnInfo(name = "name")
    lateinit var name: String

    override fun toString(): String {
        return name
    }
}