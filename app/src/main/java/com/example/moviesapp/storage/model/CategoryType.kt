package com.example.moviesapp.storage.model

enum class CategoryType(val value: Int) {
    Popular(1),
    TopRated(2),
    Upcoming(3)
}