package com.example.moviesapp.storage.model

import com.google.gson.annotations.SerializedName

class Info {

    @SerializedName("page")
    var page: Int? = 0
    @SerializedName("results")
    var movies: ArrayList<Movie>? = null
    @SerializedName("dates")
    var dates: Dates? = null
    @SerializedName("total_pages")
    var totalPages: Int? = 0
    @SerializedName("total_results")
    var totalResults: Int? = 0
}