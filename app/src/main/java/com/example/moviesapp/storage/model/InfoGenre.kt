package com.example.moviesapp.storage.model

import com.google.gson.annotations.SerializedName

class InfoGenre {
    @SerializedName("genres")
    var genres: ArrayList<Genre>? = null
}