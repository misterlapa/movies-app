package com.example.moviesapp.storage.paging

import android.arch.lifecycle.MutableLiveData
import android.arch.paging.DataSource
import com.example.moviesapp.storage.model.CategoryType
import com.example.moviesapp.storage.model.Movie
import com.example.moviesapp.storage.repository.MovieRepository

class DBMovieDataSourceFactory(private val categoryType: CategoryType, private val movieRepository: MovieRepository) :
    DataSource.Factory<Int, Movie>() {

    private lateinit var dbMovieDataSource: DBMovieDataSource
    var mutableLiveDataDB: MutableLiveData<DBMovieDataSource> = MutableLiveData()

    override fun create(): DataSource<Int, Movie> {

        dbMovieDataSource = DBMovieDataSource(categoryType, movieRepository)
        mutableLiveDataDB.postValue(dbMovieDataSource)

        return dbMovieDataSource
    }
}