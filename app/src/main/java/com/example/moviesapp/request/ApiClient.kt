package com.example.moviesapp.request

import com.example.moviesapp.storage.model.Info
import com.example.moviesapp.storage.model.InfoGenre
import com.example.moviesapp.storage.model.InfoVideo
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiClient {

    @GET("movie/popular")
    fun getPopularMovies(@Query("api_key") apiKey: String, @Query("page") page: Int): Call<Info>

    @GET("movie/top_rated")
    fun getTopRatedMovies(@Query("api_key") apiKey: String, @Query("page") page: Int): Call<Info>

    @GET("movie/upcoming")
    fun getUpcomingMovies(@Query("api_key") apiKey: String, @Query("page") page: Int): Call<Info>

    @GET("movie/{movie_id}/videos")
    fun getVideoMovies(@Path("movie_id") movieId: Long, @Query("api_key") apiKey: String): Call<InfoVideo>

    @GET("genre/movie/list")
    fun getGenreMovies(@Query("api_key") apiKey: String): Call<InfoGenre>
}