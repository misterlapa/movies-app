package com.example.moviesapp.viewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.paging.LivePagedListBuilder
import android.arch.paging.PagedList
import android.support.annotation.NonNull
import com.example.moviesapp.network.paging.NetMovieDataSource
import com.example.moviesapp.network.paging.NetMovieDataSourceFactory
import com.example.moviesapp.storage.model.Category
import com.example.moviesapp.storage.model.Movie
import com.example.moviesapp.storage.model.CategoryType
import com.example.moviesapp.storage.paging.DBMovieDataSource
import com.example.moviesapp.storage.paging.DBMovieDataSourceFactory
import com.example.moviesapp.storage.repository.MovieRepository

class MainActivityViewModel(@NonNull application: Application) : AndroidViewModel(application) {

    var lastCategory: CategoryType? = null
    private lateinit var netMovieDataSourceFactory: NetMovieDataSourceFactory
    private lateinit var dbMovieDataSourceFactory: DBMovieDataSourceFactory
    private lateinit var mutableLiveDataSourceNet: MutableLiveData<NetMovieDataSource>
    private lateinit var mutableLiveDataSourceDB: MutableLiveData<DBMovieDataSource>
    lateinit var moviePagedLive: LiveData<PagedList<Movie>>
    private var movieRepository = MovieRepository(application)
    private lateinit var livedDataCategories: LiveData<List<Category>>
    var connectedToNetwork: Boolean = false

    private var config = PagedList.Config.Builder()
        .setEnablePlaceholders(true)
        .setPageSize(10)
        .setInitialLoadSizeHint(20)
        .setPrefetchDistance(4)
        .build()

    fun buildMoviePagedLive(categoryType: CategoryType) {

        if (this.connectedToNetwork) {
            netMovieDataSourceFactory = NetMovieDataSourceFactory(categoryType, movieRepository)
            mutableLiveDataSourceNet = netMovieDataSourceFactory.mutableLiveDataNet
            moviePagedLive = LivePagedListBuilder<Int, Movie>(
                netMovieDataSourceFactory,
                config
            ).build()

        } else {
            dbMovieDataSourceFactory = DBMovieDataSourceFactory(categoryType, movieRepository)
            mutableLiveDataSourceDB = dbMovieDataSourceFactory.mutableLiveDataDB
            moviePagedLive = LivePagedListBuilder<Int, Movie>(
                dbMovieDataSourceFactory,
                config
            ).build()
        }
    }

    fun getAllCategories(): LiveData<List<Category>> {
        livedDataCategories = movieRepository.getAllCategories()
        return livedDataCategories
    }
}