package com.example.moviesapp.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.example.moviesapp.storage.model.InfoGenre
import com.example.moviesapp.storage.model.InfoVideo
import com.example.moviesapp.network.ApiUtils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DetailMovieActivityViewModel : ViewModel() {

    private var infoVideoLiveData = MutableLiveData<InfoVideo>()
    private var infoVideo: InfoVideo? = null
    private var infoGenreLiveData = MutableLiveData<InfoGenre>()
    private var infoGenre: InfoGenre? = null

    fun getInfoVideoLiveData(): MutableLiveData<InfoVideo> {
        infoVideoLiveData.value = infoVideo
        return infoVideoLiveData
    }

    fun getInfoVideoData(id: Long) {

        val apiClient = ApiUtils.getAPIClient() ?: return
        val callData = apiClient.getVideoMovies(id, ApiUtils.apiKey)

        callData.enqueue(object : Callback<InfoVideo> {
            override fun onFailure(call: Call<InfoVideo>, t: Throwable) {

            }

            override fun onResponse(call: Call<InfoVideo>, response: Response<InfoVideo>) {

                infoVideo = response.body()
                if (infoVideo == null) {
                    return
                }

                infoVideoLiveData.value = response.body()
            }
        })
    }

    fun getInfoGenreLiveData(): MutableLiveData<InfoGenre> {
        infoGenreLiveData.value = infoGenre
        return infoGenreLiveData
    }

    fun getInfoGenreData() {

        val apiClient = ApiUtils.getAPIClient() ?: return
        val callData = apiClient.getGenreMovies(ApiUtils.apiKey)

        callData.enqueue(object : Callback<InfoGenre> {
            override fun onFailure(call: Call<InfoGenre>, t: Throwable) {

            }

            override fun onResponse(call: Call<InfoGenre>, response: Response<InfoGenre>) {
                infoGenre = response.body()
                if (infoGenre == null) {
                    return
                }

                infoGenreLiveData.value = response.body()
            }
        })
    }
}