package com.example.moviesapp.adapter

import android.arch.paging.PagedListAdapter
import android.content.Context
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.example.moviesapp.R
import com.example.moviesapp.storage.model.Movie
import com.example.moviesapp.storage.model.Movie.CREATOR.CALLBACK
import kotlinx.android.synthetic.main.movie_item.view.*

class MovieAdapter(private var context: Context) :
    PagedListAdapter<Movie, MovieAdapter.MovieViewHolder>(CALLBACK) {

    lateinit var clickItem: ClickItem

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): MovieViewHolder {
        val context = p0.context
        val layoutInflater = LayoutInflater.from(context)
        val view = layoutInflater.inflate(R.layout.movie_item, p0, false)

        return MovieViewHolder(view)
    }

    override fun onBindViewHolder(p0: MovieViewHolder, p1: Int) {
        val movie = getItem(p1) ?: return

        p0.tvTitle.text = movie.originalTitle
        p0.tvRating.text = movie.voteAverage.toString()
        val imagePath = "https://image.tmdb.org/t/p/w500/" + movie.posterPath

        Glide.with(context)
            .load(imagePath)
            .placeholder(R.drawable.loading)
            .into(p0.ivMovie)

        p0.cvMovie.setOnClickListener {
            clickItem.setOnClickItem(movie)
        }
    }

    fun setOnClickItem(clickItem: ClickItem) {
        this.clickItem = clickItem
    }

    interface ClickItem {
        fun setOnClickItem(movie: Movie)
    }

    class MovieViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvRating: TextView = itemView.tvRating
        var tvTitle: TextView = itemView.tvTitle
        var ivMovie: ImageView = itemView.ivMovie
        var cvMovie: CardView = itemView.cvMovie
    }
}